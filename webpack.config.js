const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './src/main.js',
  output:{
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.bundle.js'
  },
  module:{
    loaders:[
      {
        test: /\.jsx$/,
        loader: 'babel-loader',
        query:{
          presets:['es2015']
        }
      }
    ]
  },
  stats:{
    colors:true
  },
  devtool:'source-map'
};
