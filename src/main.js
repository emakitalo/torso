import * as THREE from 'three';
import Editor from './editor/editor';

let root = document.getElementById('main');

let scene = new THREE.Scene();
let camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

let renderer = new THREE.WebGLRenderer();
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.shadowMap.autoUpdate = false;
    renderer.setSize( window.innerWidth, window.innerHeight );

root.appendChild( renderer.domElement );

let mode = 'EDIT';
let editor = new Editor(scene, camera, renderer);

window.onresize = () => {
  let width = window.innerWidth;
  let height = window.innerHeight;
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function animate(){
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
};

animate();
