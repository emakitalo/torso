import * as THREE from 'three';

export class AnimationObj{
  constructor(mesh, clock){
    this.clock = clock;
    this.mixer = new THREE.AnimationMixer(mesh);
    this.clips = mesh.animations;
  }

  update(){
    this.mixer.update(clock.getDelta());
  }

  playClip(name){
    this.mixer.clipAction(
      THREE.AnimationClip.findByName(this.clips, name)
    ).play();
  }

  stopClip(name){
    this.mixer.clipAction(
      THREE.AnimationClip.findByName(this.clips, name)
    ).stop();
  }
};

export function loadText(files, manager){
  for(let file in files){
    loader = new THREE.FileLoader(manager);
    loader.load(files[file].path, (data) => {files[file].data = data;});
  }
}

export function loadShaders(shaders, manager){
  for(let shader in shaders){
    loader = new THREE.FileLoader(manager);
    loader.load(shaders[shader].path + 'vert', (data) => {shaders[shader].vert = data;});

    loader = new THREE.FileLoader(manager);
    loader.load(shaders[shader].path + 'frag', (data) => {shaders[shader].frag = data;});
  }
}

export function loadObjects(objs, manager){
  for(let obj in objs){
    loader = new THREE.OBJLoader(manager);
    loader.load(objs[obj].path, (data) => {objs[obj].data = data.children[0];});
  }
}

export function loadTextures(textures, manager){
  for(let tex in textures){
    loader = new THREE.TextureLoader(manager);
    loader.load(textures[tex].path, (data) => {textures[tex].data = data;});
  }
}

export class Touch{
  constructor(context){
    this.context = context;
    this.touches = [];
    this.touch_direction = 0;
    this.touch_distance = 0;
    this.touch_y_treshold = 0;
    this.touch_min_distance = 10;
    this.touch_distance_in = 0.95;
    this.touch_direction_in = 0.95;
    this.tap_area = new THREE.Vector2(
      window.innerWidth - window.innerWidth * 0.50,
      window.innerHeight - window.innerHeight * 0.50 * (window.innerWidth / window.innerHeight)
    );

    this.context.addEventListener('touchstart', (e) => {
      e.preventDefault();
      let touches = e.touches;
      for(let i=0; i < touches.length; i++){
        this.touches[i] = this.cloneEvent(touches[i]);
      }
    }, false);

    this.context.addEventListener('touchend', (e) => {
      e.preventDefault();
      let touches = e.changedTouches;
      for(let i=0; i < this.touches.length; i++){
        let touch = this.touches[i];
        for(let j=0; j < touches.length; j++){
          if(touch.identifier == touches[j].identifier){
            this.touches.pop(j);
          }
        }
      }
    }, false);

    this.context.addEventListener('touchmove', (e) => {
      e.preventDefault();
      let touches = e.changedTouches;
      for(let i=0; i < touches.length; i++){
        for(let j=0; j <  this.touches.length; j++){
          let touch = this.touches[j];
          if(touch.identifier == touches[i].identifier){
            let touch_start = new THREE.Vector3(touch.clientX,touch.clientY,0);
            let touch_stop = new THREE.Vector3(touches[i].clientX,touches[i].clientY,0);
            let new_touch = this.cloneEvent(touches[i]);
                new_touch.distance += touch_start.distanceTo(touch_stop);
                new_touch.direction = touch_start.x - touch_stop.x;
                new_touch.y_treshold = touch_start.y - touch_stop.y;
            this.touches[j] = new_touch;
          }
        }
      }
    }, false);
  }

  cloneEvent(e){
    let clone = {};
    for(let val in e){
      clone[val] = e[val];
    }
    return clone;
  }
}
