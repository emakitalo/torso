import * as THREE from 'three';
import Level from './level';
import dat from 'dat.gui';

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

export default class Editor{
  constructor(scene, camera, renderer){
    // Define global shared contexts
    this.scene = scene;
    this.renderer = renderer;
    this.camera = camera;
    this.mouse = new THREE.Vector2();

    // Define additional geometries for displaying editor
    this.g_size = 15;
    let l_g = new THREE.PlaneGeometry(this.g_size, this.g_size);
    let l_m = new THREE.MeshBasicMaterial({color:0x00aa00});
        l_m.transparent = true;
        l_m.opacity = 0.35;
    let ac_g = new THREE.PlaneGeometry(1,1);
    let ac_m = new THREE.MeshBasicMaterial({color:0x990000});
    let gp_g = new THREE.PlaneGeometry(200,200);
    let gp_m = new THREE.MeshStandardMaterial({color:0xdddddd});
        gp_m.roughness = 1;

    this.ground = new THREE.Mesh(gp_g, gp_m);
    this.layer = new THREE.Mesh(l_g, l_m);
    this.grid = this.setGrid(this.g_size, this.g_size);
    this.active_level = new Level('lvl001');
    this.active_cell = new THREE.Mesh(ac_g, ac_m);
    this.active_module = undefined;
    this.active_module_id = undefined;
    this.levels = {};
    this.modules = {};
    this.state = 'EDIT';
    this.raycast = new THREE.Raycaster();
    this.pivotX = new THREE.Object3D();
    this.pivotY = new THREE.Object3D();

    // Define paths
    // this.modules_path = '/static/assets/3d/objects/json/torso_temp/';
    this.modules_path = '/static/json/';

    // Define external data loaders
    this.loading_manager = new THREE.LoadingManager();
    this.geometry_loader = new THREE.JSONLoader();

    // Define Editor lights
    this.light = new THREE.PointLight(0xffffff, 0.25, 100, 2);
    this.fill_light = new THREE.PointLight(0xffffff, 1, 100, 2);

    // Define GUI properties
    this.gui = new dat.GUI();
    this.camera_fold = this.gui.addFolder('Camera');
    this.layer_fold = this.gui.addFolder('Layer');
    this.modules_fold = this.gui.addFolder('Modules');
    this.rotationX = new THREE.Vector3(1,0,0);
    this.rotationY = new THREE.Vector3(0,1,0);

    this.camera_props = {
      'rotationX': Math.PI*1.75,
      'rotationY': 0.0,
      'rotationZ': 0.0,
      'distance': this.camera.position.z,
      'fov': 75
    };

    this.layer_props = {
      'active': 0
    };

    this.modules_props = {
      'module': ['module_001','module_002','module_003',
        'module_004','module_005','module_006','module_007'

      ]
    };

    this.active_cell.rotateX(-Math.PI*0.5);
    this.grid.position.y = -0.025;
    this.ground.position.y = -0.2;
    this.ground.receiveShadow = true;
    this.ground.rotateX(-Math.PI*0.5);
    this.layer.rotateX(-Math.PI*0.5);

    // Add geometries to scene
    this.pivotY.add(this.pivotX);
    this.pivotX.add(this.camera);
    this.scene.add(this.pivotY);
    this.scene.add(this.layer);
    this.scene.add(this.active_cell);
    this.scene.add(this.ground);
    this.scene.add(this.grid);

    // Init inits
    this.initLights();
    this.initGui();
    this.initListeners();
  }

  initListeners(){
    window.addEventListener('mousemove', (e) => {
      this.mouse = this.getMousePosition(e);
      if(this.state === 'EDIT')
        this.setActiveCell(e)
    }, false);

    window.addEventListener('click', (e) => {
      let id = this.createId(this.active_cell.position);

      if(this.active_cell.visible == true){
        if(!(id in this.active_level.grid)){
          this.addCellToGrid(id);
        }
        else{
          this.removeCellFromGrid(id);
        }
      }
    }, false);
  }

  initGui(){
    this.pivotX.setRotationFromAxisAngle(this.rotationX, this.camera_props.rotationX);
    this.camera.position.z = 10;
    this.camera.lookAt(this.grid.position);

    this.camera_fold.add(this.camera_props, 'rotationX').min(Math.PI*1.5).max(Math.PI*1.95).step(Math.PI*0.05).onChange(() => {
      this.pivotX.setRotationFromAxisAngle(this.rotationX, this.camera_props.rotationX);
    });

    this.camera_fold.add(this.camera_props, 'rotationY').min(0.0).max(Math.PI*2).step(Math.PI*0.05).onChange(() => {
      this.pivotY.setRotationFromAxisAngle(this.rotationY, this.camera_props.rotationY);
    });

    this.camera_fold.add(this.camera_props, 'distance').min(1.0).max(this.g_size * 10).step(0.1).onChange(() => {
      this.camera.position.z = this.camera_props.distance;
    });

    this.camera_fold.add(this.camera_props, 'fov').min(10).max(75).step(0.1).onChange(() => {
      this.camera.fov = this.camera_props.fov;
      this.camera.updateProjectionMatrix();
    });

    this.layer_fold.add(this.layer_props, 'active').min(0).max(10).step(1).onChange(() => {
      this.layer.position.y = this.layer_props.active - 0.025;
      this.grid.position.y = this.layer_props.active - 0.025;
    });

    this.modules_fold.add(this.modules_props, 'module', this.modules_props.module).onChange(() => {
      this.setActiveModule(this.modules_props.module);
    });
  }

  initLights(){
    this.light.position.set(0,8,0);
    this.light.castShadow = true;

    this.light.shadow.mapSize.width = 256;
    this.light.shadow.mapSize.height = 256;
    this.light.shadow.camera.near = 0.1;
    this.light.shadow.camera.far = 100;
    this.light.shadow.radius = 5;

    this.scene.add(this.light);

    this.fill_light.position.set(0,8,0);
    this.scene.add(this.fill_light);
    this.renderer.shadowMap.needsUpdate = true;
  }

  setActiveModule(id){
    if(!this.modules[id]){
      let loader = new THREE.JSONLoader();
      loader.load(
        this.modules_path + id + '.json',
        (geometry, materials) => {
          let mesh = new THREE.Mesh(geometry, materials[1]);
              mesh.castShadow = true;
              mesh.receiveShadow = false;

          this.modules[this.modules_props.module] = mesh;
          this.active_module = this.modules[this.modules_props.module];
        }, 
        (xhr) => {
          console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
        },
        (err) => {
          console.log( 'An error happened' + err);
        }
        );
      this.modules[this.modules_props.module] = loader;
    }
    else{
      this.active_module = this.modules[this.modules_props.module];
    }
  }

  addCellToGrid(id){
    let x = this.active_cell.position.x;
    let y = this.active_cell.position.y;
    let z = this.active_cell.position.z;
    let mesh = this.active_module.clone();
        mesh.castShadow = true;
        mesh.receiveShadow = false;
        mesh.position.set(x, y, z);

    this.active_level.grid[id] = {
      mesh : mesh
    };

    this.scene.add(this.active_level.grid[id].mesh);
    this.renderer.shadowMap.needsUpdate = true;
  }

  removeCellFromGrid(id){
    this.scene.remove(this.active_level.grid[id].mesh);
    delete this.active_level.grid[id];
    this.renderer.shadowMap.needsUpdate = true;
  }

  createId(position){
    return position.x.toString() + '_' + position.y.toString() + '_' + position.z.toString();
  }

  getMousePosition(e){
    return  new THREE.Vector2(
      (e.clientX / this.renderer.domElement.clientWidth) * 2 - 1,
      -(e.clientY / this.renderer.domElement.clientHeight) * 2 + 1
    );
  }

  setActiveCell(){
    this.raycast.setFromCamera(this.mouse, this.camera);
    let i = this.raycast.intersectObject(this.layer);

    if(i.length > 0){
      this.active_cell.visible = true;
      this.active_cell.position.set(
        Math.round(i[0].point.x), 
        Math.round(i[0].point.y) + 0.02, 
        Math.round(i[0].point.z)
      );
    }
    else{
      this.active_cell.visible = false;
    }
  }

  setGrid(size, divs){
    return new THREE.GridHelper(size, divs);
  }

  setActiveLevel(name){
    this.active_level = this.renderLevel(this.levels[name]);
  }

  loadLevels(url){
    this.levels =  fetch(url).then((re) => {
      this.state = 'READY';
      return JSON.parse(re);
    });
  }

  renderLevel(data){
    let level = new Level(data);
    if(data != 'undefined'){
    }
    return level;
  }
};
